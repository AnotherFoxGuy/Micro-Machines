﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BaseCar : MonoBehaviour
{
    public Transform ResetPos;
    protected Rigidbody Rigidbody;
    protected ParticleSystem TireSmoke;
    protected int TriggerIndex;

    public GameUI Ui;
    public int RacePosition;
    public int RaceLap = 3;
    public Trigger NextTrigger;
    public int NextTriggerIndex;
    internal bool HasFinished;

    private ParticleSystem _fireworks;



    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        TireSmoke = transform.Find("Tire smoke").GetComponentInChildren<ParticleSystem>();
        TireSmoke.Stop();
        if (transform.Find("Fireworks"))
            _fireworks = transform.Find("Fireworks").GetComponent<ParticleSystem>();
        var gui = GetComponentInChildren<GameUI>();
        if (gui)
            Ui = gui;    
    }

    protected void ResetCar()
    {
        try
        {
            transform.position = Random.onUnitSphere + ResetPos.position;
            transform.rotation = ResetPos.rotation;
        }
        catch (Exception e)
        {
            print(e);
            transform.position = Vector3.up;
            transform.rotation = Quaternion.identity;
        }

        Rigidbody.velocity = Vector3.zero;
        Rigidbody.angularVelocity = Vector3.zero;
    }

    protected static bool V3Equal(Vector3 a, Vector3 b)
    {
        return Vector3.SqrMagnitude(a - b) < 0.0001;
    }

    public virtual void FireFireworks()
    {
        if (_fireworks)
            _fireworks.Play();
    }

    public virtual void NewRound(int roundNum)
    {
    }
    
}
