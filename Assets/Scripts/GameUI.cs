﻿using System.Collections;
using System.Collections.Generic;
using MarkLight;
using MarkLight.Views.UI;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public Vector3 TextOffset = new Vector3(7.5f, 7.5f);

    private BaseCar _car;

    public float RaceTime;
    public GameObject ArrowMesh;
    public Vector3 PointTo;
    private TextMesh _textMesh;
    private Transform _camTransform;
    private Transform _lockToObject;

    private void Awake()
    {
        _lockToObject = transform.parent.transform;
        _car = GetComponentInParent<CarControl>();
        _textMesh = GetComponent<TextMesh>();
        _camTransform = GetComponentInParent<CarControl>().CamTransform.GetComponentInChildren<Camera>().transform;
    }


    private void Update()
    {
        if (_car.HasFinished)
        {
            _textMesh.text =
                $@" Finished !!!
Position: {_car.RacePosition} 
Time: {RaceTime / 60 % 60:00}:{RaceTime % 60:00}:{RaceTime * 1000 % 1000:000}";
        }
        else
        {
            _textMesh.text =
                $@"Position: {_car.RacePosition} 
Lap: {_car.RaceLap} 
Time: {RaceTime / 60 % 60:00}:{RaceTime % 60:00}:{RaceTime * 1000 % 1000:000}";
        }

        ArrowMesh.transform.LookAt(PointTo);
        transform.rotation = Quaternion.LookRotation(transform.position - _camTransform.position);
        transform.position = _lockToObject.position + TextOffset;
    }
}