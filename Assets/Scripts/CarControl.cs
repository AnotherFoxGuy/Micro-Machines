using System;
using System.Collections.Generic;
using FMOD.Studio;
using UnityEngine;

public class CarControl : BaseCar
{
    public string DriveAxes = "Vertical";
    public string SteerAxes = "Horizontal";
    public string ResetButton = "Reset";

    public Transform CamTransform;
    public int Speed;
    public int RotateSpeed;
    public int MaxSpeed;

    private Vector3 _lookAtPos;
    private bool _inDrift;

    public FMODUnity.StudioEventEmitter CarEngineEE;
    public FMODUnity.StudioEventEmitter CarDriftEE;
    [FMODUnity.EventRef] public string CarCrashEvent;


    private void Update()
    {
        if (Input.GetButtonDown(ResetButton) || transform.position.y < -50)
            ResetCar();
        CarEngineEE.SetParameter("RPM", Rigidbody.velocity.magnitude / MaxSpeed);
    }

    private void OnCollisionEnter(Collision other)
    {
        FMODUnity.RuntimeManager.PlayOneShot(CarCrashEvent, transform.position);
    }

    private void FixedUpdate()
    {
        _lookAtPos = transform.position - transform.forward * 20;

        if (!V3Equal(CamTransform.position, _lookAtPos))
        {
            var dif = CamTransform.position - _lookAtPos;
            dif /= 10;
            CamTransform.position -= dif;
        }

        if (HasFinished)
        {
            Rigidbody.AddRelativeForce(-transform.InverseTransformDirection(Rigidbody.velocity) * 10);
            return;
        }

        var down = transform.TransformDirection(Vector3.down);
        Debug.DrawRay(transform.position, down, Color.red);

        if (Physics.Raycast(transform.position, down, 2.5f))
        {
            if (Math.Abs(Input.GetAxis(DriveAxes)) > float.Epsilon)
            {
                var locVelZ = transform.InverseTransformDirection(Rigidbody.velocity).z;
                if (Math.Abs(locVelZ) < MaxSpeed)
                    Rigidbody.AddRelativeForce(0, 0, -(Input.GetAxis(DriveAxes) * Speed));
            }


            if (Math.Abs(Input.GetAxis(SteerAxes)) > float.Epsilon)
            {
                transform.Rotate(Vector3.up, RotateSpeed * Input.GetAxis(SteerAxes));
                Rigidbody.angularVelocity = Vector3.zero;
            }


            var locVelX = transform.InverseTransformDirection(Rigidbody.velocity).x;
            Rigidbody.AddRelativeForce(-locVelX * 15, 0, 0);

            SetDrift(Math.Abs(locVelX) > 10);
        }
        else
        {
            SetDrift(false);
            Rigidbody.AddForce(0, -750, 0);
        }
    }

    public override void FireFireworks()
    {
        TireSmoke.Stop();
        //_carDriftState.stop(STOP_MODE.ALLOWFADEOUT);
        base.FireFireworks();
    }

    private void SetDrift(bool state)
    {
        if (_inDrift == state)
            return;
        if (state)
        {
            TireSmoke.Play();
            //CarDriftEE.SetParameter("DriftSpeed", 1);
            //CarDriftEE.Play();
        }

        else
        {
            TireSmoke.Stop();
            //CarDriftEE.SetParameter("DriftSpeed", 0);
        }

        _inDrift = state;
    }
}