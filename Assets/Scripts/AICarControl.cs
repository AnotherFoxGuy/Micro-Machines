﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AICarControl : BaseCar
{
    public int Speed;
    public int RotateSpeed;
    public int MaxSpeed;

    private float _intSpeed;
    private List<Trigger> _triggers;
    private int _triggerIndex;

    // Use this for initialization
    private void Start()
    {
        _triggers = GameObject.Find("GameManager").GetComponent<GameManager>().Triggers;
        _intSpeed = MaxSpeed / 1.5f;
    }

    private void Update()
    {
        if (transform.position.y < -50)
            ResetCar();
    }

    private void FixedUpdate()
    {
        if (HasFinished)
        {
            Rigidbody.AddRelativeForce(-transform.InverseTransformDirection(Rigidbody.velocity) * 10);
            return;
        }

        var down = transform.TransformDirection(Vector3.down);
        Debug.DrawRay(transform.position, down, Color.red);

        if (Physics.Raycast(transform.position, down, 2.5f))
        {
            var locVelZ = transform.InverseTransformDirection(Rigidbody.velocity).z;
            if (Math.Abs(locVelZ) < _intSpeed)
                Rigidbody.AddRelativeForce(0, 0, -Speed);


            //Steer
            if (Vector3.Distance(NextTrigger.transform.position, transform.position) < 15)
            {
                _triggerIndex++;
                if (_triggerIndex == _triggers.Count)
                    _triggerIndex = 0;
            }

            var targetDir = transform.position - NextTrigger.transform.position;
            var step = RotateSpeed * Time.deltaTime;
            var newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
            newDir.y = 0;
            Debug.DrawRay(transform.position, newDir * 10, Color.green);
            transform.rotation = Quaternion.LookRotation(newDir);

            Rigidbody.angularVelocity = Vector3.zero;

            Debug.DrawLine(transform.position, NextTrigger.transform.position, Color.blue);

            //part
            var locVelX = transform.InverseTransformDirection(Rigidbody.velocity).x;
            Rigidbody.AddRelativeForce(-locVelX * 17, 0, 0);

            if (Math.Abs(locVelX) > 10)
                TireSmoke.Play();
            else
                TireSmoke.Stop();
        }
        else
        {
            TireSmoke.Stop();
            Rigidbody.AddForce(0, -750, 0);
        }
    }

    public override void NewRound(int roundNum)
    {
        if (roundNum == 2)
            _intSpeed = MaxSpeed;
    }
}