﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControlerOLD : MonoBehaviour
{
    public CheckPoint[] CheckPoints;
    private List<CheckPoint> CheckPoints_list;

    public Text StateText;
    public float Timer;
    public ArrowControler ArrowControler;

    private void Awake()
    {
        CheckPoints_list = new List<CheckPoint>(CheckPoints);
    }

    public void TriggerCallback(CheckPoint id)
    {
        Timer += id.Time;
        var next = CheckPoints_list.IndexOf(id);
        if (next > 0)
            ArrowControler.PointTo = CheckPoints_list[next + 1].transform.position;
    }

    private void Update()
    {
        Timer -= Time.deltaTime;
    }
}