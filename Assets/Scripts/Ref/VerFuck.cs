﻿using UnityEngine;

public class VerFuck : MonoBehaviour
{
    private Rigidbody _vRigidbody;
    public float AddSpeed;
    public float MaxSpeed;

    private void Start()
    {
        _vRigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Debug.DrawRay(transform.position + Vector3.up, Vector3.down * 2);
    }

    private void FixedUpdate()
    {
        if (_vRigidbody.velocity.magnitude < MaxSpeed &&
            Physics.Raycast(transform.position + Vector3.up, Vector3.down, 2))
            _vRigidbody.AddRelativeForce(Vector3.back * AddSpeed);
    }
}