﻿using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private GameControlerOLD _controler;
    public float Time;

    private void Awake()
    {
        _controler = GameObject.Find("GlobalControl").GetComponent<GameControlerOLD>();
    }

    private void OnTriggerEnter(Collider other)
    {
        _controler.TriggerCallback(this);
    }
}