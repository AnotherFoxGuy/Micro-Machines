﻿using UnityEngine;

public class ArrowControler : MonoBehaviour
{
    public Vector3 PointTo;

    // Update is called once per frame
    private void Update()
    {
        transform.LookAt(PointTo);
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y);
    }
}