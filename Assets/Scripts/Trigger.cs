using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    private GameManager _gameManager;

    private void Awake()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //print(other);
        var cc = other.GetComponent<CarControl>();
        if (cc)
            _gameManager.CarTrigger(this,cc);
        
        var acc = other.GetComponent<AICarControl>();
        if(acc)
            _gameManager.CarTrigger(this,acc);
    }
}