﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private bool _hasStarted = false;
    public Text InfoText;
    public Button ReturnButton;

    public List<Trigger> Triggers;

    private readonly List<BaseCar> _allCars = new List<BaseCar>();

    private Trigger _nextTrigger;

    //private int _nextTriggerIndex;
    private float _intTime = -3;

    private List<CarControl> _playerCars;

    private void Awake()
    {
        _allCars.AddRange(FindObjectsOfType<BaseCar>());
        _playerCars = _allCars.OfType<CarControl>().ToList();
        _nextTrigger = Triggers[0];
        ReturnButton.gameObject.SetActive(false);
        ReturnButton.onClick.AddListener(ReturnToMainMenu);

        foreach (var c in _allCars)
        {
            c.NextTrigger = Triggers[0];
            //c.Ui.PointTo = Triggers[0].transform.position;
            c.GetComponent<Rigidbody>().isKinematic = true;
        }
    }


    private void Start()
    {
        foreach (var c in _playerCars)
            c.Ui.PointTo = Triggers[0].transform.position;
    }

    void ReturnToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void Update()
    {
        _intTime += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Escape))
            ReturnToMainMenu();

        if (!_hasStarted)
        {
            if (Mathf.CeilToInt(_intTime) != 0)
                InfoText.text = $"{Mathf.Abs(Mathf.Ceil(_intTime))}";
            else
                InfoText.text = "Go !!!";

            if (_intTime > 0)
            {
                _hasStarted = true;
                InfoText.gameObject.SetActive(false);
                foreach (var c in _allCars)
                    c.GetComponent<Rigidbody>().isKinematic = false;
            }
        }


        _allCars.Sort(SortCars);

        for (var i = 0; i < _playerCars.Count; i++)
        {
            if (_playerCars[i].HasFinished)
                return;

            _playerCars[i].Ui.RaceTime = _intTime;
            _playerCars[i].RacePosition = _allCars.IndexOf(_playerCars[i]) + 1;
        }
    }

    private int SortCars(BaseCar a, BaseCar b)
    {
        if (a.RaceLap != b.RaceLap)
            return a.RaceLap.CompareTo(b.RaceLap);
        if (a.NextTriggerIndex != b.NextTriggerIndex)
            return b.NextTriggerIndex.CompareTo(a.NextTriggerIndex);
        return Vector2.Distance(a.transform.position, a.NextTrigger.transform.position)
            .CompareTo(Vector2.Distance(b.transform.position, a.NextTrigger.transform.position));
    }

    public void CarTrigger(Trigger t, BaseCar car)
    {
        if(car.HasFinished)
            return;
        
        if (_nextTrigger == t)
        {
            var tni = Triggers.IndexOf(_nextTrigger) + 1;
            if (tni == Triggers.Count)
                tni = 0;

            //print($"Next trigger: {tni}");

            _nextTrigger = Triggers[tni];
            //_nextTriggerIndex = tni;
        }

        if (car.NextTrigger == t)
        {
            var tni = Triggers.IndexOf(car.NextTrigger) + 1;
            if (tni == Triggers.Count)
            {
                tni = 0;
                car.RaceLap--;
                car.NewRound(car.RaceLap);
                if (car.RaceLap == 0)
                {
                    car.HasFinished = true;
                    car.FireFireworks();
                    if (_playerCars.Count(c => !c.HasFinished) == 0)
                        ReturnButton.gameObject.SetActive(true);
                }
            }

            if (car is CarControl)
                car.Ui.PointTo = Triggers[tni].transform.position;

            car.NextTrigger = Triggers[tni];
            car.NextTriggerIndex = tni;
            car.ResetPos = t.transform;
        }
    }


    private void OnDrawGizmos()
    {
        for (var i = 0; i < Triggers.Count - 1; i++)
            Debug.DrawLine(Triggers[i].transform.position, Triggers[i + 1].transform.position);
    }
}