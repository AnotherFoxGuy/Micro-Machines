using MarkLight.Views.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : UIView
{
    public void StartSPGame()
    {
        Debug.Log("StartSPGame() called.");
        SceneManager.LoadScene("SPGame");
    }

    public void StartMPGame()
    {
        Debug.Log("StartMPGame() called.");
        SceneManager.LoadScene("MPGame");
        //SceneManager.LoadScene("controllerTest");
    }

    public void Quit()
    {
        Debug.Log("Quit() called.");
        Application.Quit();
    }
}